Feature: Functional Tests-Omnius Application 

@TC01 @functional @Regression
Scenario Outline: TC001 Login Flow 
	Given Enter Username as <username> 
	And Enter password as <password>
	When User click the login button
	Then Verify if User is able to login to the application
	
	Examples: 
		|username|password|
		|testuser|testuser|
		
@functional @TCO2 @Regression
Scenario Outline: TC002 Login Flow 
	Given Enter Username as <username> 
	And Enter password as <password>
	When User click the login button
	Then Verify if User is NOT able to login to the application
	
	Examples: 
		|username|password|
		|INVALIDUSER|testuser|
		|testuser|INVALIDUSER|
		
@TC03 @functional @Regression
Scenario Outline: TC003 Verify if collection is created 
	Given Enter Username as <username> 
	And Enter password as <password>
	And User click the login button
	And User click Annotate link
	And CLick New Collection provided in the webpage
	When User enter the <CollectionName> And Click Create button 
	Then verify if the <CollectionName> is created successfully
	
	Examples: 
		|username|password|CollectionName|
		|testuser|testuser|OMNITEST_NITHISH1|
		|testuser|testuser|OMNITEST_NITHISH2|

@TC04 @functional @Regression
Scenario Outline: TC004 Verify if collection is sorted as expected 
	Given Enter Username as <username> 
	And Enter password as <password>
	And User click the login button
	And User click Annotate link
	And User Search collection Name as <CollectionName>
	When User click on Sort icon 
	Then verify if the collection list is sorted as expected
	
	Examples: 
		|username|password|CollectionName|
		|testuser|testuser|OmniUs|
		
@TC05 @functional @Regression
Scenario Outline: TC005 Verify if the valid file formats are uploaded 
	Given Enter Username as <username> 
	And Enter password as <password>
	And User click the login button
	And User click Annotate link
	And User Search collection Name as <CollectionName>
	Then verify if only valid files from <directory> are uploaded
	
	Examples: 
		|username|password|CollectionName|directory|
		|testuser|testuser|nithish|C:\\Users\\n.vinolin.stephen\\Documents\\Selenium_Training\\Maven\\DocumentExplorer\\test-data|
		
@TC006 @functional @Regression
Scenario Outline: TC006 Verify if the uploaded files getting sorted as expected 
	Given Enter Username as <username> 
	And Enter password as <password>
	And User click the login button
	And User click Annotate link
	And User Search collection Name as <CollectionName>
	When User click on Collections Name and upload new documents from the <directory> 
	When User click on the sort icon in the Documents upload section
	Then Verify if the file names is sorted as expected
	
	Examples: 
		|username|password|CollectionName|directory|
		|testuser|testuser|Nithish|C:\\Users\\n.vinolin.stephen\\Documents\\Selenium_Training\\Maven\\DocumentExplorer\\test-data|
	
	
	
	
	
