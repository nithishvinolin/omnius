package runner;

import org.testng.annotations.BeforeTest;

import com.omnius.testng.api.base.Annotations;

import cucumber.api.CucumberOptions;

@CucumberOptions(features="src/test/java/features/DocumentExplorer.Feature",
tags= {"@TC006"},
plugin= {"pretty","html:target/cucumber-html-report"},
				 glue={"com/omnius"}, 
				 monochrome=true)

public class VerifyFilesSort extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC006_VerifyFilesSort";
		testcaseDec = "Verify if the files are sorted as expected when user clicks on the sort icon";
		author = "Nithish";
		category = "Coding Challenge";
	} 

}




