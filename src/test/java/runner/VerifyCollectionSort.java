package runner;

import org.testng.annotations.BeforeTest;

import com.omnius.testng.api.base.Annotations;

import cucumber.api.CucumberOptions;

@CucumberOptions(features="src/test/java/features/DocumentExplorer.Feature",
tags= {"@TC04"},
plugin= {"pretty","html:target/cucumber-html-report"},
				 glue={"com/omnius"}, 
				 monochrome=true)

public class VerifyCollectionSort extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC004_VerifyCollectionSort";
		testcaseDec = "Verify if Collection names are sorted when user click on the sort icon";
		author = "Nithish";
		category = "Coding Challenge";
	} 

}




