package runner;

import org.testng.annotations.BeforeTest;

import com.omnius.testng.api.base.Annotations;

import cucumber.api.CucumberOptions;

@CucumberOptions(features="src/test/java/features/DocumentExplorer.Feature",
tags= {"@TCO2"},
plugin= {"pretty","html:target/cucumber-html-report"},
				 glue={"com/omnius"}, 
				 monochrome=true)

public class OmniusLogin_NegativeFlow extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC002_OmniUs Login Negative Flow";
		testcaseDec = "Verify if user is NOT able to login with invalid credentials";
		author = "Nithish";
		category = "Coding Challenge";
	} 

}




