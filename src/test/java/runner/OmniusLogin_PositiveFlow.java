package runner;

import org.testng.annotations.BeforeTest;

import com.omnius.testng.api.base.Annotations;

import cucumber.api.CucumberOptions;

@CucumberOptions(features="src/test/java/features/DocumentExplorer.Feature",
tags= {"@TC01"},
plugin= {"pretty","html:target/cucumber-html-report"},
				 glue={"com/omnius"}, 
				 monochrome=true)

public class OmniusLogin_PositiveFlow extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC001_OmniUs Login Positive Flow";
		testcaseDec = "Verify if user is able to login successfully";
		author = "Nithish";
		category = "Coding Challenge";
	} 

}




