package runner;

import org.testng.annotations.BeforeTest;

import com.omnius.testng.api.base.Annotations;

import cucumber.api.CucumberOptions;

@CucumberOptions(features="src/test/java/features/DocumentExplorer.Feature",
tags= {"@TC05"},
plugin= {"pretty","html:target/cucumber-html-report"},
				 glue={"com/omnius"}, 
				 monochrome=true)

public class VerifyFileUpload extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC005_VerifyDocumentsUpload";
		testcaseDec = "Verify if Documents uploaded successfully";
		author = "Nithish";
		category = "Coding Challenge";
	} 

}




