package runner;

import org.testng.annotations.BeforeTest;

import com.omnius.testng.api.base.Annotations;

import cucumber.api.CucumberOptions;

@CucumberOptions(features="src/test/java/features/DocumentExplorer.Feature",
tags= {"@TC03"},
plugin= {"pretty","html:target/cucumber-html-report"},
				 glue={"com/omnius"}, 
				 monochrome=true)

public class CreateAndVerifyCollection extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC003_CreateAndVerifyNewCollection";
		testcaseDec = "Create a New Collection and verify if the collection created successfully";
		author = "Nithish";
		category = "Coding Challenge";
	} 

}




