package com.omnius.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.omnius.testng.api.base.Annotations;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

public class LoginPage extends Annotations{ 

	public LoginPage() {
		PageFactory.initElements(driver, this); 
	}
	@CacheLookup
	@FindBy(how=How.ID, using="username") 
	WebElement eleUserName;
	@FindBy(how=How.ID, using="password") 
	WebElement elePassWord;
	@FindBy(how=How.NAME, using="login") 
	WebElement eleLogin;
	
	@Given("Enter Username as (.*)")
	public LoginPage enterUserName(String data) {
		clearAndType(eleUserName, data);  
		return this; 
	}
	@Given("Enter password as (.*)")
	public LoginPage enterPassWord(String data) {
		clearAndType(elePassWord, data); 
		return this; 
	}
	@When("User click the login button")
	public TrainerConsole clickLogin() {
		click(eleLogin);  
		return new TrainerConsole();
	}
}


