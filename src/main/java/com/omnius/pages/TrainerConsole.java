package com.omnius.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.omnius.testng.api.base.Annotations;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class TrainerConsole extends Annotations{ 

	@Then("Verify if User is able to login to the application")
	public TrainerConsole verifySuccessfulLogin() throws InterruptedException {
		Thread.sleep(5000);
		WebElement loginSuccess = locateElement("xpath","//chunk[text()='Console']");
		if(loginSuccess.isDisplayed()==true) {
			System.out.println("PASSED- User successfully logged in");
		}
		else {
			System.out.println("FAILED- User DID NOT logged in successfully");
		}

		return this;
		
	}
	@Then("Verify if User is NOT able to login to the application")
	public TrainerConsole verifyFailureLogin() throws InterruptedException {
		Thread.sleep(5000);
		WebElement loginFailure = locateElement("class","kc-feedback-text");
		if(loginFailure.isDisplayed()==true) {
			System.out.println("PASSED- Failure message displayed as expected");
					}
		else {
			System.out.println("FAILED- Failure message is expecteds");
		}
		return this;
		
	}
	@Given("User click Annotate link")
	public TrainerDocExplorer clickaAnnotate() throws InterruptedException {
		
		WebElement eleAnnonate=locateElement("xpath","//label[text()='Annotate']");
		click(eleAnnonate); 
		Thread.sleep(5000);
		return new TrainerDocExplorer();
	}
	

}







