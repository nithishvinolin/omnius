package com.omnius.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.omnius.testng.api.base.Annotations;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import utils.SortIgnoreCase;

public class TrainerDocExplorer extends Annotations{ 
	String CollValName;
	String givenCollectionName;
	
	public  List<String> collectionName = new ArrayList<String>();
	public  List<String> fileNames = new ArrayList<String>();
	
	public Collection<String> getCollectionName(int noOfCol) {
		for(int i=0;i<noOfCol;i++) {
			
		String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"+ "0123456789"+ "abcdefghijklmnopqrstuvxyz"; 
		StringBuilder sb = new StringBuilder(); 
		for (int j = 0; j < 5; j++) { 
			int rIndex = (int)(AlphaNumericString.length() * Math.random()); 
		sb.append(AlphaNumericString.charAt(rIndex)); 
		
		} 
		collectionName.add(sb.toString()+"OmniusTest_Nithish");

	}
		return collectionName;		
	}
	public Collection<String> getFileNames(String fileName){
		fileNames.add(fileName);
		return fileNames;
	}
	
	@Then("verify if only valid files from (.*) are uploaded")
	public TrainerDocExplorer verifyFileUploadBasedOnExtension(String fileName) throws InterruptedException, AWTException {

		List<WebElement> CollectionNames = locateElements("xpath","//echo-table-standalone[contains(@class,'echoTableHeightFull picnicHeightLimiter')]//table//tr/td//block");
		
		for (WebElement webElement : CollectionNames) {
			webElement.click();
			Thread.sleep(3000);
			
			if(locateElements("xpath","//button[text()='Assign a new document']").size()!=0) {
				File folder = new File(fileName);
				File[] files = folder.listFiles();
				for(int i = 0; i < files.length;i++) {
					WebElement eleUpload = locateElement("xpath","//button[text()='Upload']");
					String filesList = "";
				    filesList += (i != 0 ?"":"") + files[i].getAbsolutePath();
				    eleUpload.click();
				    Thread.sleep(5000);
					  //Clipboard
						 StringSelection strSel = new StringSelection(filesList);
						 Toolkit.getDefaultToolkit().getSystemClipboard().setContents(strSel, null);
						Robot robot=new Robot();
						robot.keyPress(KeyEvent.VK_CONTROL);
						robot.keyPress(KeyEvent.VK_V);
						robot.keyRelease(KeyEvent.VK_CONTROL);
						
						robot.keyPress(KeyEvent.VK_ENTER);
						robot.keyRelease(KeyEvent.VK_ENTER);
						Thread.sleep(5000);
						try {
							if(filesList.contains("pdf") && driver.findElementByTagName("succeeded").isDisplayed()) {
								System.out.println("PASSED: pdf File format accepted");
								
							}
						} catch (Exception pdf) {
							System.out.println("PDF Check Failed");
						}
							
							try {
							if(filesList.contains("jpg") && driver.findElementByTagName("succeeded").isDisplayed()) {
								System.out.println("PASSED: JPG File format accepted");
							}
							}catch (Exception jpg) {
								System.out.println("JPG Check Failed");
							}
								try {
							if(filesList.contains("png") && driver.findElementByTagName("succeeded").isDisplayed()) {
								System.out.println("PASSED: PNG File format accepted");
							}
							}
							catch (Exception png) {
								System.out.println("PNG Check Failed");
							}
								
								try {
							if(filesList.contains("tif") && driver.findElementByTagName("succeeded").isDisplayed()) {
								System.out.println("PASSED: tif File format accepted");
							}
								}catch (Exception tif) {
									System.out.println("TIF Check Failed");
								}
									
									try {
							if(filesList.contains("docx") && driver.findElementByTagName("rejected").isDisplayed()) {
								System.out.println("PASSED: docx File format accepted");
							}
									} catch (Exception docx) {
										System.out.println("docx Check Failed");
									}
							try {
							if(filesList.contains("xlsx") && driver.findElementByTagName("rejected").isDisplayed()) {
								System.out.println("PASSED: xlsx File format NOT accepted");
							}
							} catch (Exception xlsx) {
								System.out.println("xlsx Check Failed");
							}
							
							WebElement clearUpload = locateElement("xpath","//chunk[text()='clear finished']");
							clearUpload.click();
							
							Thread.sleep(3000);
				
			}
			break;
			}
		
	}
		return this;
	}
	
	@Given("CLick New Collection provided in the webpage")
	public TrainerDocExplorer AddNewCollection()  {
			WebElement newCollection = locateElement("class","iconPlus");
			driver.executeScript("arguments[0].click();", newCollection);
			return this;
	}
	@When("User enter the (.*) And Click Create button")
	public TrainerDocExplorer EnterCollName(String cName) throws InterruptedException  {
			WebElement eleEnterCollName = locateElement("xpath","//input[@placeholder='Name']");
			WebElement eleCreate = locateElement("xpath","//button[text()='Create']");
			WebElement eleCancel = locateElement("class","picnicButton");
			clearAndType(eleEnterCollName, cName);
			click(eleCreate); 
			List<WebElement> errMsg = locateElements("xpath","//paragraph[text()='Something went unexpectedly wrong. Try again. If the problem persists contact your administrator.']");

			if(errMsg.size()!=0) {
				Actions actions = new Actions(driver);
				actions.moveToElement(eleCancel).click().build().perform();
			}
		
		return this;
	}
	
	@Then("verify if the (.*) is created successfully")
    public TrainerDocExplorer VerifyCollNameCreated(String data) throws InterruptedException  {
		
		WebElement eleSearchBox = locateElement("xpath","//echo-table-standalone[contains(@class,'echoTableHeightFull picnicHeightLimiter')]//input[1]");
		clearAndType(eleSearchBox, data);
		Thread.sleep(5000);
		List<WebElement> dataVal = locateElements("xpath","//echo-table-standalone[contains(@class,'echoTableHeightFull picnicHeightLimiter')]//table//tr/td//block");
		String givenCollectionName = dataVal.get(0).getText();
		//System.out.println(givenCollectionName);
		if (givenCollectionName.equals(data)){
			System.out.println("The Given Collection Name is successfully created");
	}
	else {
		System.out.println("Test Failed: Given collection name not Created");
			}
		return this;
    	
    }
	
@Given("User Search collection Name as (.*)")
public TrainerDocExplorer SearchCollectionName(String data) throws InterruptedException {
	WebElement eleSearchBox = locateElement("xpath","//echo-table-standalone[contains(@class,'echoTableHeightFull picnicHeightLimiter')]//input[1]");
	clearAndType(eleSearchBox, data);
	Thread.sleep(3000);
return this;	
}

@When("User click on Sort icon")
public TrainerDocExplorer performCollectionSort() throws InterruptedException {
	
	WebElement CollectionSortIcon = locateElement("xpath","//icon[@class='iconSort tableHeaderIconAction']");
	click(CollectionSortIcon);
	Thread.sleep(3000);
	return this;
	
}
@Then("verify if the collection list is sorted as expected")
public TrainerDocExplorer verifyCollectionSorted() {
	List<String> CollectionList = new ArrayList<String>();
	List<WebElement> CollectionNames = locateElements("xpath","//echo-table-standalone[contains(@class,'echoTableHeightFull picnicHeightLimiter')]//table//td[1]//block");
	for (WebElement cName : CollectionNames) {
		String CollectionName = cName.getText();
		CollectionList.add(CollectionName);
	}
	List<String> tmp= new ArrayList<String>(CollectionList);
	
	Collections.sort(tmp,new SortIgnoreCase());
	
	boolean sortResult = CollectionList.equals(tmp);
	if(sortResult==true) {
		System.out.println("Passed- The Collections names are sorted as expected");
	}
	else {
		System.out.println("Failed- The Collections names are NOT sorted as expected");
	}
	return this;
}
@When("User click on the sort icon in the Documents upload section")
public TrainerDocExplorer usertClickDocumenSortIcon() throws InterruptedException {
	WebElement eleDocSort = locateElement("xpath","(//icon[@class='iconSort tableHeaderIconAction'])[2]");
	eleDocSort.click();
	Thread.sleep(3000);
	return this;	
}

@Then("Verify if the file names is sorted as expected")
public TrainerDocExplorer verifyIfDocsSorted() throws InterruptedException, AWTException {

	List<String> docList = new ArrayList<String>();
	List<WebElement> docNames = locateElements("xpath","//table[@class='picnicTableReactiveRows']//tr/td[2]//a|//table[@class='picnicTableReactiveRows']//tr/td[2]//empty");
	for (WebElement cName : docNames) {
		String fileNames = cName.getText();
		docList.add(fileNames);
	}
	List<String> tmp= new ArrayList<String>(docList);
	Collections.sort(tmp,new SortIgnoreCase());	
	boolean sortResult = docList.equals(tmp);
	if(sortResult==true) {
		System.out.println("Passed- The File names are sorted as expected");
	}
	else {
		System.out.println("Failed- The File names are NOT sorted as expected");
	}
	return this;
	}
	
	


@When("User click on Collections Name and upload new documents from the (.*)")
public TrainerDocExplorer userSelectCollectionAndUploadMultipleFiles(String fileName) throws InterruptedException, AWTException {
	List<WebElement> CollectionNames = locateElements("xpath","//echo-table-standalone[contains(@class,'echoTableHeightFull picnicHeightLimiter')]//table//tr/td//block");
	for (WebElement webElement : CollectionNames) {
		if(locateElements("xpath","//paragraph[text()='Forbidden.']").size()==0) {
			webElement.click();
			Thread.sleep(3000);
			WebElement eleUpload = locateElement("xpath","//button[text()='Upload']");
		    eleUpload.click();
		    Thread.sleep(5000);
			StringSelection strSel = new StringSelection(fileName);
					 Toolkit.getDefaultToolkit().getSystemClipboard().setContents(strSel, null);
					Robot robot=new Robot();
					robot.keyPress(KeyEvent.VK_CONTROL);
					robot.keyPress(KeyEvent.VK_V);
					robot.keyRelease(KeyEvent.VK_CONTROL);
					
					robot.keyPress(KeyEvent.VK_ENTER);
					robot.keyRelease(KeyEvent.VK_ENTER);
					
					robot.keyPress(KeyEvent.VK_SHIFT);
					robot.keyPress(KeyEvent.VK_TAB);
					robot.keyRelease(KeyEvent.VK_SHIFT);
					
					robot.keyPress(KeyEvent.VK_CONTROL);
					robot.keyPress(KeyEvent.VK_A);
					robot.keyRelease(KeyEvent.VK_CONTROL);
					
					robot.keyPress(KeyEvent.VK_ENTER);
					robot.keyRelease(KeyEvent.VK_ENTER);
					
					Thread.sleep(5000);
						
						WebElement clearUpload = locateElement("xpath","//chunk[text()='clear finished']");
						clearUpload.click();
						
						Thread.sleep(3000);
			break;
		}
		
	}

	return this;
	
}

}



	







