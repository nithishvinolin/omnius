package com.omnius.testcases;

import java.awt.AWTException;
import java.util.Collection;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.omnius.pages.LoginPage;
import com.omnius.pages.TrainerDocExplorer;
import com.omnius.testng.api.base.Annotations;
// Annotations == ProjectBase
public class VerifyFilesSort extends Annotations{
	 //private TrainerDocExplorer getCollectionNM;
	@BeforeTest
	public void setData() {
		testcaseName = "TC006_VerifyFilesSort";
		testcaseDec = "Verify if the files are sorted as expected when user clicks on the sort icon";
		author = "Nithish";
		category = "Coding Challenge";
		excelFileName = "TestFileUpload";
		testFilePath="C:\\Users\\n.vinolin.stephen\\Documents\\Selenium_Training\\Maven\\DocumentExplorer\\test-data";
		
	} 

	@Test(dataProvider="fetchData") 
	public void verifyCollSort(String uName, String pwd,String collectionNm) throws InterruptedException, AWTException {
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd) 
		.clickLogin()
		.clickaAnnotate()
		.SearchCollectionName(collectionNm)
		.userSelectCollectionAndUploadMultipleFiles(testFilePath)
		.usertClickDocumenSortIcon()
		.verifyIfDocsSorted();
		
		
	}
	
}






