package com.omnius.testcases;

import java.util.Collection;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.omnius.pages.LoginPage;
import com.omnius.pages.TrainerDocExplorer;
import com.omnius.testng.api.base.Annotations;
// Annotations == ProjectBase
public class CreateAndVerifyCollection extends Annotations{
	 private TrainerDocExplorer getCollectionNM;
	@BeforeTest
	public void setData() {
		testcaseName = "TC003_CreateAndVerifyNewCollection";
		testcaseDec = "Create a New Collection and verify if the collection created successfully";
		author = "Nithish";
		category = "Coding Challenge";
		excelFileName = "TC003";
	} 

	@Test(dataProvider="fetchData") 
	public void CreateCollections(String uName, String pwd,String folderNm) throws InterruptedException {
		getCollectionNM= new TrainerDocExplorer();
		Collection<String> collectionName = getCollectionNM.getCollectionName(5);
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd) 
		.clickLogin()
		.clickaAnnotate();
		for (String cName : collectionName) {
			new TrainerDocExplorer()
			.AddNewCollection()
			.EnterCollName(cName)
			.VerifyCollNameCreated(cName);
		}
		
		
	}
	
}






