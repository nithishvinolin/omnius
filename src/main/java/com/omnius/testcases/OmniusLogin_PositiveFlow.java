package com.omnius.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.omnius.pages.LoginPage;
import com.omnius.testng.api.base.Annotations;
// Annotations == ProjectBase
public class OmniusLogin_PositiveFlow extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC001_OmniUs Login Positive Flow";
		testcaseDec = "Verify if user is able to login successfully";
		author = "Nithish";
		category = "Coding Challenge";
		excelFileName = "TC001";
	} 

	@Test(dataProvider="fetchData") 
	public void loginOmniUs(String uName, String pwd) throws InterruptedException {
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd) 
		.clickLogin()
		.verifySuccessfulLogin();
		
	}
	
}






