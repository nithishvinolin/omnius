package com.omnius.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.omnius.pages.LoginPage;
import com.omnius.testng.api.base.Annotations;
// Annotations == ProjectBase
public class OmniusLogin_NegativeFlow extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC002_OmniUs Login Negative Flow";
		testcaseDec = "Verify if user is NOT able to login with invalid credentials";
		author = "Nithish";
		category = "Coding Challenge";
		excelFileName = "TC002";
	} 

	@Test(dataProvider="fetchData") 
	public void loginOmniUs(String uName, String pwd) throws InterruptedException {
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd) 
		.clickLogin()
		.verifyFailureLogin();
		
	}
	
}






