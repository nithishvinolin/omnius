package com.omnius.testcases;

import java.util.Collection;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.omnius.pages.LoginPage;
import com.omnius.pages.TrainerDocExplorer;
import com.omnius.testng.api.base.Annotations;
// Annotations == ProjectBase
public class VerifyCollectionSort extends Annotations{
	 private TrainerDocExplorer getCollectionNM;
	@BeforeTest
	public void setData() {
		testcaseName = "TC004_VerifyCollectionSort";
		testcaseDec = "Verify if Collection names are sorted when user click on the sort icon";
		author = "Nithish";
		category = "Coding Challenge";
		excelFileName = "TC003";
	} 

	@Test(dataProvider="fetchData") 
	public void verifyCollSort(String uName, String pwd,String folderNm) throws InterruptedException {
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd) 
		.clickLogin()
		.clickaAnnotate()
		.SearchCollectionName(folderNm)
		.performCollectionSort()
		.verifyCollectionSorted();
		
		
	}
	
}






